import { fileURLToPath, URL } from 'url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

const env = import.meta.env

// https://vitejs.dev/config/
export default defineConfig({
	server: {
		// host: 'localhost'
		// proxy: {
		//   "/api": {
		// 	target: "https://api.hivegroupinc.com",
		// 	changeOrigin: true,
		// 	secure: false,
		// 	rewrite: (path) => path.replace(/^\/supmgnt/, ""),
		//   },
		// },
		// '/': {
		// 	target: 'http://localhost/'
		// },
	},
	plugins: [vue()],
	resolve: {
		alias: {
		'@': fileURLToPath(new URL('./src', import.meta.url))
		}
	},
	build: {
		target: 'esnext'
	},
	publicPath: ''
})
