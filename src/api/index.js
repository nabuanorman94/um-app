import axios from 'axios'

const env = import.meta.env

const headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Access-Control-Allow-Origin': '*' // Could work and fix the previous problem, but not in all APIs
}

export function auth_login (payload) {
	return axios.post(env.VITE_API_LOGIN_ENDPOINT, payload)
}

export function auth_logout (payload) {
	return axios.post(env.VITE_API_LOGOUT_ENDPOINT, payload)
}

export function get_customers (payload) {
	return axios.post(payload.api + env.VITE_API_GET_CUSTOMER_INFO_API, payload.data)
}

export function get_escalations (payload) {
	return axios.post(env.VITE_API_GET_ESCALATION_API, payload)
}

export function get_all_users () {
	return axios.post('https://cauth3.forwardbpo.com/noauthsvc/getActiveUserList/json')
}

// export function getCustomers () {
//   console.log(data)
// }

// export function createMessage ({ text, thread }, cb) {
//   const timestamp = Date.now()
//   const id = 'm_' + timestamp
//   const message = {
//     id,
//     text,
//     timestamp,
//     threadID: thread.id,
//     threadName: thread.name,
//     authorName: 'Evan'
//   }
//   setTimeout(function () {
//     cb(message)
//   }, LATENCY)
// }