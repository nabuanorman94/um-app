import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../views/Dashboard.vue'

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			name: 'dashboard',
			component: Dashboard,
			meta: {
				metaTitle: 'Dashboard',
				authRequired: true
			}
		}, {
			path: '/login',
			name: 'login',
			// route level code-splitting
			// this generates a separate chunk (About.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import('../views/auth/Login.vue'),
			meta: {
				metaTitle: 'Login',
				forAuth: true
			}
		}, {
			path: '/user-management',
			name: 'user_management',
			component: () => import('../views/UserManagement.vue'),
			meta: {
				metaTitle: 'User Management',
				authRequired: true
			}
		}, {
			path: '/admin-utility',
			name: 'admin_utility',
			component: () => import('../views/AdminUtility.vue'),
			meta: {
				metaTitle: 'Admin Utility',
				authRequired: true
			}
		}, {
			path: '/user-journey',
			name: 'user_journey',
			component: () => import('../views/UserJourney.vue'),
			meta: {
				metaTitle: 'User Journey',
				authRequired: true
			}
		}, {
			path: '/profile',
			name: 'my_profile',
			component: () => import('../views/MyProfile.vue'),
			meta: {
				metaTitle: 'My Profile',
				authRequired: true
			}
		}
		// , {
		// 	path: '/user-management/customer',
		// 	name: 'um_customer',
		// 	component: Customer,
		// 	meta: {
		// 		metaTitle: 'Customer',
		// 		authRequired: true
		// 	}
		// }, {
		// 	path: '/user-management/escalation',
		// 	name: 'um_escalation',
		// 	component: Escalation,
		// 	meta: {
		// 		metaTitle: 'Escalation',
		// 		authRequired: true
		// 	}
		// }, {
		// 	path: '/admin-utility/access',
		// 	name: 'au_access',
		// 	component: Access,
		// 	meta: {
		// 		metaTitle: 'Access',
		// 		authRequired: true
		// 	}
		// }, {
		// 	path: '/admin-utility/app-settings',
		// 	name: 'au_app_settings',
		// 	component: AppSettings,
		// 	meta: {
		// 		metaTitle: 'AppSettings',
		// 		authRequired: true
		// 	}
		// },
	]
})

export default router
