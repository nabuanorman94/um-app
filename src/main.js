import { createApp } from 'vue'
import { useStore } from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store'
import './index.css'
import mitt from 'mitt'
import { initialize } from './helpers/auth-helper'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'

const emitter = mitt();

initialize(store, router)

const app = createApp(App)
  .use(router)
  .use(store)
  .component('v-select', vSelect)

app.config.globalProperties.emitter = emitter;

await router.isReady()
app.mount('#app')