export const user = state => state.user
export const isAuthenticated = state => state.isAuthenticated
export const apiServer = state => state.apiServer

export const loading = state => state.loading
export const isDark = state => state.isDark
export const isSidebarOpen = state => state.isSidebarOpen
export const isNotificationsPanelOpen = state => state.isNotificationsPanelOpen
export const isSearchPanelOpen = state => state.isSearchPanelOpen
export const isMobileSubMenuOpen = state => state.isMobileSubMenuOpen
export const isMobileMainMenuOpen = state => state.isMobileMainMenuOpen
export const toast = state => state.toast
export const cardTypes = state => state.cardTypes

export const customerList = state => state.customerList
export const customerData = state => state.customerData
export const customerToAuthenticatePayload = state => state.customerToAuthenticatePayload
export const searchCustomerPayload = state => state.searchCustomerPayload

export const userList = state => state.userList
export const groupList = state => state.groupList

// Dummy Data
export const customers = state => state.customers

// export const currentThread = state => {
//   return state.currentThreadID
//     ? state.threads[state.currentThreadID]
//     : {}
// }

// export const currentMessages = state => {
//   const thread = currentThread(state)
//   return thread.messages
//     ? thread.messages.map(id => state.messages[id])
//     : []
// }

// export const unreadCount = ({ threads }) => {
//   return Object.keys(threads).reduce((count, id) => {
//     return threads[id].lastMessage.isRead ? count : count + 1
//   }, 0)
// }

// export const sortedMessages = (state, getters) => {
//   const messages = getters.currentMessages
//   return messages.slice().sort((a, b) => a.timestamp - b.timestamp)
// }