import { createStore, createLogger } from 'vuex'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

const myUser = JSON.parse(localStorage.getItem('um_user'))

const state = {
    user: (myUser)? myUser : null,
    isAuthenticated: (myUser)? true : false,
    apiServer: (myUser)? myUser.api : null,
    // userInitials: (myUser)? myUser.name.split(' ').map(name => name[0]).join('').toUpperCase() : null,
    validate: {
        username: [
          	v => !!v || 'Username is required'
        ],
			email: [
			v => !!v || 'E-mail is required',
			v => /.+@.+/.test(v) || 'E-mail must be valid'
        ],
        password: [
			v => !!v || 'Password is required',
			v => (v)? v.length >= 8 || 'Password must be at least 8 characters' : ''
        ],
    },
    loading: true,
    isDark: false,
    isSidebarOpen: true,
    isNotificationsPanelOpen: false,
    isSearchPanelOpen: false,
    isMobileSubMenuOpen: false,
    isMobileMainMenuOpen: false,
    toast: {
		type: null,
		title: null
    },
    cardTypes: [
		{ value: 'visa', label: 'Visa' },
		{ value: 'mastercard', label: 'Mastercard' },
		{ value: 'citibank', label: 'Citibank' },
		{ value: 'chase', label: 'Chase' },
		{ value: 'american express', label: 'American Express' },
		{ value: 'capital one', label: 'Capital One' },
		{ value: 'bank of america', label: 'Bank of America' },
    ],
    customers: [], // for dummy data
	
    customerList: [],
	customerData: null,
    // customerData: { "loginId": 635, "customer": "cus_IZc3NV5zD3BT1i", "user": { "loginId": 635, "email": "angelab.mcfer@gmail.com", "firstname": "Angela", "lastname": "Fernandez" }, "subscriptionId": "sub_IZc3ZRFvfqNiXQ", "timestamp": { "date": "2020-12-15 01:59:57.244264", "timezone_type": 3, "timezone": "GMT" }, "dateint": 1607997597, "updated": { "date": "2020-12-15 01:59:57.928102", "timezone_type": 3, "timezone": "GMT" }, "subscription": { "id": "sub_IZc3ZRFvfqNiXQ", "object": "subscription", "billing_cycle_anchor": 1610589593, "collection_method": "charge_automatically", "created": 1607997593, "current_period_end": 1610589593, "current_period_start": 1607997593, "customer": "cus_IZc3NV5zD3BT1i", "days_until_due": null, "default_payment_method": { "id": "pm_1HySpPL0qtDOfocnzBBdWMhx", "object": "payment_method", "card": { "brand": "visa", "country": "US", "exp_month": 12, "exp_year": 2024, "funding": "credit", "generated_from": null, "last4": "4242" }, "created": 1607997592, "customer": "cus_IZc3NV5zD3BT1i", "livemode": false, "type": "card" }, "latest_invoice": "in_1HySpSL0qtDOfocnUMZVkGqO", "livemode": false, "metadata": [], "plan": { "id": "price_1HlSqDL0qtDOfocnPiIu2NoA", "object": "plan", "active": true, "amount": 1995, "amount_decimal": "1995", "billing_scheme": "per_unit", "created": 1604899377, "currency": "usd", "interval": "month", "interval_count": 1, "product": "prod_IMBCEFN3Z0xkEj", "usage_type": "licensed" }, "quantity": 1, "start_date": 1607997593, "status": "trialing", "trial_end": 1610589593, "trial_start": 1607997593 }, "updatedBy": 635 },
	customerToAuthenticatePayload: {
		accessToken: (myUser)? myUser.accessToken : null,
		email: null, 
		otp: null,
		customerID: null
	},
	searchCustomerPayload: {
		accessToken: (myUser)? myUser.accessToken : null,
		email: null, 
		last4: null,
		type: 'visa'
	},
	userList: [],
	groupList: []
}

export default createStore({
    state,
    getters,
    actions,
    mutations,
    plugins: process.env.NODE_ENV !== 'production'
      ? [createLogger()]
      : []
})