import * as api from '../api'

// export const getAllMessages = ({ commit }) => {
//   api.getAllMessages(messages => {
//     commit('receiveAll', messages)
//   })
// }

// export const sendMessage = ({ commit }, payload) => {
//   api.createMessage(payload, message => {
//     commit('receiveMessage', message)
//   })
// }

export const updateLoading = ({commit}, payload) => {
    commit('UPDATE_LOADING', payload)
}

export const getTheme = ({commit}) => {
    if (window.localStorage.getItem('dark')) {
        commit('SET_THEME', JSON.parse(window.localStorage.getItem('dark')))
    } else {
        commit('SET_THEME', !!window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches)
    }
}

export const login = ({commit}, payload) => {
    return api.auth_login(payload);
}

export const logout = ({commit}, payload) => {
    return api.auth_logout(payload);
}


export const getCustomerList = ({commit}, payload) => {
    return api.get_customers(payload)
}

export const getUserList = ({commit}) => {
    return api.get_all_users()
}


export const getCustomers = ({commit}, payload) => {
    // api.getCustomers(payload, customers => {
    //     commit('UPDATE_CUSTOMERS', customers)
    // })

    const mockCustomer = [
        {
            "id": 1,
            "user": {
                "username": "dprice0",
                "first_name": "Daniel",
                "last_name": "Price",
                "email": "dprice0@blogs.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "3 Toban Park",
            "city": "Pocatello",
            "state": "Idaho",
            "age": 54
        }, {
            "id": 2,
            "user": {
                "username": "dgreen1",
                "first_name": "Doris",
                "last_name": "Green",
                "email": "dgreen1@elpais.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "4210 Portage Trail",
            "city": "Mobile",
            "state": "Alabama",
            "age": 71
        }, {
            "id": 3,
            "user": {
                "username": "njohnston2",
                "first_name": "Nicholas",
                "last_name": "Johnston",
                "email": "njohnston2@tiny.cc"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "94 Hanson Trail",
            "city": "Brooklyn",
            "state": "New York",
            "age": 89
        }, {
            "id": 4,
            "user": {
                "username": "rlee3",
                "first_name": "Ronald",
                "last_name": "Lee",
                "email": "rlee3@shareasale.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "54915 Ludington Park",
            "city": "Salt Lake City",
            "state": "Utah",
            "age": 44
        }, {
            "id": 5,
            "user": {
                "username": "jcox4",
                "first_name": "Jose",
                "last_name": "Cox",
                "email": "jcox4@sitemeter.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "9 South Parkway",
            "city": "Glendale",
            "state": "Arizona",
            "age": 18
        }, {
            "id": 6,
            "user": {
                "username": "eward5",
                "first_name": "Ernest",
                "last_name": "Ward",
                "email": "eward5@imageshack.us"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "72 Shelley Plaza",
            "city": "Whittier",
            "state": "California",
            "age": 6
        }, {
            "id": 7,
            "user": {
                "username": "phall6",
                "first_name": "Patrick",
                "last_name": "Hall",
                "email": "phall6@github.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "27773 Orin Hill",
            "city": "Fort Lauderdale",
            "state": "Florida",
            "age": 88
        }, {
            "id": 8,
            "user": {
                "username": "lnichols7",
                "first_name": "Lawrence",
                "last_name": "Nichols",
                "email": "lnichols7@home.pl"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "03444 Harbort Trail",
            "city": "Syracuse",
            "state": "New York",
            "age": 89
        }, {
            "id": 9,
            "user": {
                "username": "hperkins8",
                "first_name": "Heather",
                "last_name": "Perkins",
                "email": "hperkins8@addtoany.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "1086 Myrtle Pass",
            "city": "White Plains",
            "state": "New York",
            "age": 79
        }, {
            "id": 10,
            "user": {
                "username": "jwoods9",
                "first_name": "Jonathan",
                "last_name": "Woods",
                "email": "jwoods9@blogs.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "5697 Leroy Street",
            "city": "Akron",
            "state": "Ohio",
            "age": 5
        }, {
            "id": 11,
            "user": {
                "username": "nwelcha",
                "first_name": "Nancy",
                "last_name": "Welch",
                "email": "nwelcha@so-net.ne.jp"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "386 Sachs Terrace",
            "city": "Tucson",
            "state": "Arizona",
            "age": 71
        }, {
            "id": 12,
            "user": {
                "username": "emillerb",
                "first_name": "Eric",
                "last_name": "Miller",
                "email": "emillerb@chicagotribune.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "7233 Melvin Center",
            "city": "Pittsburgh",
            "state": "Pennsylvania",
            "age": 27
        }, {
            "id": 13,
            "user": {
                "username": "hallenc",
                "first_name": "Heather",
                "last_name": "Allen",
                "email": "hallenc@ucoz.ru"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "199 David Street",
            "city": "Wichita",
            "state": "Kansas",
            "age": 6
        }, {
            "id": 14,
            "user": {
                "username": "wlittled",
                "first_name": "Walter",
                "last_name": "Little",
                "email": "wlittled@goodreads.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "9019 Truax Road",
            "city": "Bakersfield",
            "state": "California",
            "age": 63
        }, {
            "id": 15,
            "user": {
                "username": "jthompsone",
                "first_name": "Janice",
                "last_name": "Thompson",
                "email": "jthompsone@fotki.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "4 Lillian Drive",
            "city": "Cleveland",
            "state": "Ohio",
            "age": 80
        }, {
            "id": 16,
            "user": {
                "username": "kgeorgef",
                "first_name": "Kathryn",
                "last_name": "George",
                "email": "kgeorgef@nydailynews.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "0133 Northwestern Lane",
            "city": "Pueblo",
            "state": "Colorado",
            "age": 50
        }
    ];

    commit('UPDATE_CUSTOMERS', mockCustomer)
}

export const getEscalations = ({commit}, payload) => {
    // api.getCustomers(payload, customers => {
    //     commit('UPDATE_CUSTOMERS', customers)
    // })

    const mockCustomer = [
        {
            "id": 1,
            "user": {
                "username": "dprice0",
                "first_name": "Daniel",
                "last_name": "Price",
                "email": "dprice0@blogs.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "3 Toban Park",
            "city": "Pocatello",
            "state": "Idaho",
            "age": 54
        }, {
            "id": 2,
            "user": {
                "username": "dgreen1",
                "first_name": "Doris",
                "last_name": "Green",
                "email": "dgreen1@elpais.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "4210 Portage Trail",
            "city": "Mobile",
            "state": "Alabama",
            "age": 71
        }, {
            "id": 3,
            "user": {
                "username": "njohnston2",
                "first_name": "Nicholas",
                "last_name": "Johnston",
                "email": "njohnston2@tiny.cc"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "94 Hanson Trail",
            "city": "Brooklyn",
            "state": "New York",
            "age": 89
        }, {
            "id": 4,
            "user": {
                "username": "rlee3",
                "first_name": "Ronald",
                "last_name": "Lee",
                "email": "rlee3@shareasale.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "54915 Ludington Park",
            "city": "Salt Lake City",
            "state": "Utah",
            "age": 44
        }, {
            "id": 5,
            "user": {
                "username": "jcox4",
                "first_name": "Jose",
                "last_name": "Cox",
                "email": "jcox4@sitemeter.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "9 South Parkway",
            "city": "Glendale",
            "state": "Arizona",
            "age": 18
        }, {
            "id": 6,
            "user": {
                "username": "eward5",
                "first_name": "Ernest",
                "last_name": "Ward",
                "email": "eward5@imageshack.us"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "72 Shelley Plaza",
            "city": "Whittier",
            "state": "California",
            "age": 6
        }, {
            "id": 7,
            "user": {
                "username": "phall6",
                "first_name": "Patrick",
                "last_name": "Hall",
                "email": "phall6@github.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "27773 Orin Hill",
            "city": "Fort Lauderdale",
            "state": "Florida",
            "age": 88
        }, {
            "id": 8,
            "user": {
                "username": "lnichols7",
                "first_name": "Lawrence",
                "last_name": "Nichols",
                "email": "lnichols7@home.pl"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "03444 Harbort Trail",
            "city": "Syracuse",
            "state": "New York",
            "age": 89
        }, {
            "id": 9,
            "user": {
                "username": "hperkins8",
                "first_name": "Heather",
                "last_name": "Perkins",
                "email": "hperkins8@addtoany.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "1086 Myrtle Pass",
            "city": "White Plains",
            "state": "New York",
            "age": 79
        }, {
            "id": 10,
            "user": {
                "username": "jwoods9",
                "first_name": "Jonathan",
                "last_name": "Woods",
                "email": "jwoods9@blogs.com"
            },
            "issue": "Open",
            "escalationDate": "01-01-1991",
            "address": "5697 Leroy Street",
            "city": "Akron",
            "state": "Ohio",
            "age": 5
        }, {
            "id": 11,
            "user": {
                "username": "nwelcha",
                "first_name": "Nancy",
                "last_name": "Welch",
                "email": "nwelcha@so-net.ne.jp"
            },
            "issue": "Resolved",
            "escalationDate": "01-01-1991",
            "address": "386 Sachs Terrace",
            "city": "Tucson",
            "state": "Arizona",
            "age": 71
        }, {
            "id": 12,
            "user": {
                "username": "emillerb",
                "first_name": "Eric",
                "last_name": "Miller",
                "email": "emillerb@chicagotribune.com"
            },
            "issue": "Resolved",
            "escalationDate": "01-01-1991",
            "address": "7233 Melvin Center",
            "city": "Pittsburgh",
            "state": "Pennsylvania",
            "age": 27
        }, {
            "id": 13,
            "user": {
                "username": "hallenc",
                "first_name": "Heather",
                "last_name": "Allen",
                "email": "hallenc@ucoz.ru"
            },
            "issue": "Resolved",
            "escalationDate": "01-01-1991",
            "address": "199 David Street",
            "city": "Wichita",
            "state": "Kansas",
            "age": 6
        }, {
            "id": 14,
            "user": {
                "username": "wlittled",
                "first_name": "Walter",
                "last_name": "Little",
                "email": "wlittled@goodreads.com"
            },
            "issue": "Resolved",
            "escalationDate": "01-01-1991",
            "address": "9019 Truax Road",
            "city": "Bakersfield",
            "state": "California",
            "age": 63
        }, {
            "id": 15,
            "user": {
                "username": "jthompsone",
                "first_name": "Janice",
                "last_name": "Thompson",
                "email": "jthompsone@fotki.com"
            },
            "issue": "Resolved",
            "escalationDate": "01-01-1991",
            "address": "4 Lillian Drive",
            "city": "Cleveland",
            "state": "Ohio",
            "age": 80
        }, {
            "id": 16,
            "user": {
                "username": "kgeorgef",
                "first_name": "Kathryn",
                "last_name": "George",
                "email": "kgeorgef@nydailynews.com"
            },
            "issue": "Resolved",
            "escalationDate": "01-01-1991",
            "address": "0133 Northwestern Lane",
            "city": "Pueblo",
            "state": "Colorado",
            "age": 50
        }
    ];

    commit('UPDATE_CUSTOMERS', mockCustomer)
}