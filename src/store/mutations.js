const env = import.meta.env

export default {
    SET_USER(state, payload) {
        state.user =  Object.assign({}, payload)
        localStorage.setItem('um_user', JSON.stringify(state.user))
    },
    UPDATE_IS_AUTHENTICATED(state, payload) {
        state.loading = payload
    },
    LOGOUT(state) {
        localStorage.removeItem('um_user')
        state.user = null
        state.isAuthenticated = false
    },
    UPDATE_LOADING(state, payload) {
        state.loading = payload
    },
    SET_THEME(state, payload) {
        state.isDark = payload
    },
    TOGGLE_THEME(state) {
        state.isDark = !state.isDark
    },
    TOGGLE_SIBEBAR(state) {
        state.isSidebarOpen = !state.isSidebarOpen
    },
    TOGGLE_NOTIFICATION_PANNEL(state) {
        state.isNotificationsPanelOpen = !state.isNotificationsPanelOpen
    },
    TOGGLE_SEARCH_PANNEL(state) {
        state.isSearchPanelOpen = !state.isSearchPanelOpen
    },
    TOGGLE_MOBILE_SUB_MENU(state) {
        state.isMobileSubMenuOpen = !state.isMobileSubMenuOpen
    },
    TOGGLE_MOBILE_MAIN_MENU(state) {
        state.isMobileMainMenuOpen = !state.isMobileMainMenuOpen
    },
    SET_TOAST(state, payload) {
        state.toast =  Object.assign(state.toast, payload)
    },
    UPDATE_CUSTOMERS(state, payload) { // Sample only
        state.customers = payload
    },
    SET_API_SERVER(state, payload) {
        switch(payload) {
            case 'US':
                state.apiServer = (process.env.NODE_ENV !== 'production')? env.VITE_API_US_STAGE_SERVER : env.VITE_API_US_PROD_SERVER
                break;
            case 'APAC':
                state.apiServer =  (process.env.NODE_ENV !== 'production')? env.VITE_API_APAC_STAGE_SERVER : env.VITE_API_APAC_PROD_SERVER
                break;
        }
    },

    UDPATE_CUSTOMER_TO_AUTHENTICATE_PAYLOAD(state, payload) {
        state.customerToAuthenticatePayload =  Object.assign(state.customerToAuthenticatePayload, payload)
    },
    UPDATE_CUSTOMER_LIST(state, payload) {
        state.customerList = payload
    },
    UPDATE_CUSTOMER_DATA(state, payload) {
        state.customerData =  Object.assign({}, payload)
    },
    UPDATE_CUSTOMER_SEARCH_PAYLOAD(state, payload) {
        state.searchCustomerPayload =  Object.assign(state.searchCustomerPayload, payload)
    },

    UPDATE_USER_LIST(state, payload) {
        state.userList = payload
    },
    UPDATE_GROUP_LIST(state, payload) {
        state.groupList = payload
    }
}