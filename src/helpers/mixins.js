// import moment from 'moment-timezone'

// export default {
//   methods: {
//     getImgUrlFromStorage(pic) {
//       if(pic && !pic.includes('blob:')) {
//         var url = pic.replace('public', '/storage')
//         return process.env.VUE_APP_APIURL + url + '?' + moment().unix()
//       } else {
//         return pic
//       }
//     },
//     getImgUrlFromStorageToDownload(pic) {
//       var url = pic.replace('public', '/storage')
//       return process.env.VUE_APP_APIURL + url
//     },
//     trimString(string, value) {
//       return string.slice(0, value) + (string.length > value ? "..." : "");
//     },
//     formatDate(date, format) {
//       return moment(date).format(format)
//     },
//     getRetaliveTime(date) {
//       // console.log(date)
//       var defaultDate = moment.tz(date, process.env.VUE_APP_TZDEFAULT);
//       var newDate = defaultDate.clone().tz(process.env.VUE_APP_TZCURRENT).startOf('minutes').fromNow()
//       // return moment.tz(date, "Asia/Manila").startOf('hour').fromNow()
//       return newDate
//     },
//     getCurrencyFormat(value) {
//       if(value && value > 0) {
//         return "$ " + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
//       } else {
//         return "$ " + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
//       }
//     },
//     showAll(routeName) {
//       this.$router.push({name: routeName})
//     },
//     strReplace(orig_str, remove_str, replace_str) {
//       return orig_str.replace(remove_str, replace_str)
//     },
//     removeTimestamp(filename) {
//       var array = filename.split('_')
//       return filename.replace(array[0] + '_', '')
//     }
//   }
// }