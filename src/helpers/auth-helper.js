import axios from 'axios'

export function initialize(store, router) {
	const isAuthenticated = store.getters.isAuthenticated;
	
	router.beforeEach((to, from, next) => {

		if(to.name == 'login') {
			if(isAuthenticated) {
				next({
					name: 'dashboard'
				});
			} else {
				next();
			}
		} else {
			if(to.matched.some(record => record.meta.authRequired) && isAuthenticated) {
				next();
			} else {
				next({
					name: 'login'
				});
			}
		}

		// // if(to.path == '/') {
		// //   next({
		// //     name: 'home'
		// //   });
		// // } else {
		//   if(to.matched.some(record => record.meta.authRequired)) {
		//     if(!store.getters['AuthStore/isAuthenticated']) {
		//       next({
		//         name: 'login'
		//       });
		//     } else {
		//       next();
		//     }
		//   } 
		//   else {
		//     if(store.getters['AuthStore/isAuthenticated']) {
		//       // var user = store.getters['AuthStore/user']
		//       // console.log(user)
		//       next({
		//         name: 'home'
		//       });
		//     } else {
		//       next();
		//     }
		//   }
		// // }
	});

	// axios.interceptors.response.use(null, (error)=> {
	//   if(error.response.status == 401) {
	//       store.commit('AuthStore/logout');
	//       // router.push('/login');
	//       // window.location.href = '/login'
	//   }
	//   return Promise.reject(error);
	// });

	// axios.interceptors.request.use(request => {
	// 	console.log('Starting Request', JSON.stringify(request, null, 2))
	// 	return request
	// })
	  
	axios.interceptors.response.use(response => {
		console.log(response)
		if(response.data.isAuthorized && !response.data.isAuthorized.valid) {
			const message = response.data.isAuthorized.message

			setTimeout(() => {
				store.commit('SET_TOAST', {
					type: 'warning',
					title: message
				})
				store.commit('LOGOUT')
				// router.push('/login')
				window.location.href = '/login'
			}, 10)
		}
		
		return response
	})

	// const guest_token = store.getters['AuthStore/guestToken'] // from Guest account
	// axios.defaults.headers.common["Authorization"] = (store.getters['AuthStore/user'])? 'Bearer '+store.getters['AuthStore/user'].token: 'Bearer '+guest_token;
}